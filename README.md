# Description

This repository contains modules for Snakemake, which are included as wrapper within Eilslabs-Workflows.
We defines a module as the execution of (single/few) tools applied for a specific use-case and with defined input and output files.

# Structure

Our module naming shema orders modules into specific sub-directories.
The schema is based on the functionality of modules and it uses four levels:

1.  Primary input file
2.  Primary output file
3.  Optional level to structure modules into groups (e.g. QC)
4.  Name of the primary tool used within wrapper (e.g bwa) or short term for describing use case (e.g. align, sort)

In cases of multiple input/output files, the module developer is free to define primary input/output files.
Currently the schema included the following formats for input output files (other may be included any time).
Thereby, packed files are referred as their unpacked versions.

* fastq: reads in standard [fastq](https://en.wikipedia.org/wiki/FASTQ_format) format
* fasta: any sequence(s) in [fasta](https://en.wikipedia.org/wiki/FASTA_format) format 
* alignment: BAM or SAM file
* statistics: a table or any other format which contains relevent statistics
* variants: VCF etc. reporting on genomic variants
* undefined: if a specific format is not required or if input or out files are missing 

# Usage

Within a Snakemake workflow, modules are included by [wrapper](https://snakemake.readthedocs.io/en/stable/snakefiles/modularization.html?highlight=wrapper) directive.
You need to refer this module repository using '--wrapper-prefix https://gitlab.com/eilslabs_workflows/modules/raw/' argument during Snakemake call. 
Tests with snakemake docker quay.io/snakemake/snakemake:v5.5.2

# Test data

TODO

# Add new module

1.  Add wrapper.py Python3 script at specific path defined by primary input and output files.
2.  Define a small test workflow using a Snakefile. This test workflow should use small test files stored within test_data directories.
3.  Test your wrapper locally using Snakefile. Run test using snakefile and preferably official snakemake docker container. use local 
4.  Add wrapper to test script; this will be used for automated tests

# External scripts

TODO

# Install locally

It is recommend to use the online module repository at gitlab.
There might be situations where you would like to install a local version of the module repository.
We recommmend to install it into a tag specific folder.
This way, the workflow rules can refer to a specific version of the module repository.

```bash
TAG=1.0.0
WORKFLOWS=/data/analysis/workflows
git clone gitlab:eilslabs_workflows/modules.git
cd modules
git checkout $TAG
mkdir $WORKFLOWS/modules
mkdir $WORKFLOWS/modules/$TAG
mv * $WORKFLOWS/modules/$TAG
mv .git* $WORKFLOWS/modules/$TAG
```