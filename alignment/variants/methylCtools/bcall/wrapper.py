"""Snakemake wrapper for bisulfite/bcall"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os

try:
    script = "%s/bcall.py" % (snakemake.params.get("scriptdir", "scripts"))
    shell("sleep 5;python {script:q} {snakemake.input.positions:q} {snakemake.input.bam:q} - | bgzip > {snakemake.output.call:q}")
    shell("sleep 5;tabix -s 1 -b 2 -e 2 {snakemake.output.call:q}")

except Exception as error:
    print(error)
    sys.exit(1)

