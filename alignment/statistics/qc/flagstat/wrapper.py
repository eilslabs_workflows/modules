"""Snakemake wrapper for sambamba flagstat"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell

try:
    shell("sleep 5;sambamba flagstat -t 1 {snakemake.input.bam:q} > {snakemake.output.flagstat:q}")
except Exception as error:
    print(error)
    sys.exit(1)
