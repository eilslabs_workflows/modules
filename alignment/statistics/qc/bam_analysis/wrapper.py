"""Snakemake wrapper for bam analysis (DKFZ script)"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os

try:
    script = "%s/flags_isizes_PEaberrations.pl" % (snakemake.params.get("scriptdir", "scripts"))
    # run shell
    shell("sleep 5;sambamba view {snakemake.input.bam:q} | perl {script:q} " +
        " -i /dev/stdin" +
        " -a {snakemake.output.diffchrom_matrix:q}" +
        " -o {snakemake.output.diffchrom_statistics:q}" +
        " -b {snakemake.output.isizes_matrix:q}" +
        " -m {snakemake.output.isizes_statistic:q}" +
        " -f {snakemake.output.extended_flagstat:q}" +
        " -c {snakemake.params.chrom_sizes:q}" +
        " -p {snakemake.params.insert_size_limit}")
except Exception as error:
    print(error)
    sys.exit(1)
