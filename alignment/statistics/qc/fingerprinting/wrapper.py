"""Snakemake wrapper for fingerprinting (DKFZ script)"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os

try:
    script = "%s/bsnp.py" % (snakemake.params.get("scriptdir", "scripts"))
    shell("sleep 5;python {script:q} {snakemake.params.positions:q} {snakemake.input.bam:q} > {snakemake.output.fingerprinting:q}")
except Exception as error:
    print(error)
    sys.exit(1)
