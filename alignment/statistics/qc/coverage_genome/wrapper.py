"""Snakemake wrapper for coverage_genome (DKFZ script)"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os

try:
    script_coverage = "%s/coverageQc" % (snakemake.params.get("scriptdir", "scripts"))
    script_groups =  "%s/groupedGenomeCoverages.pl" % (snakemake.params.get("scriptdir", "scripts"))
    # parameter
    qual_cutoff = snakemake.params.get("qual_cutoff", "10")
    #TODO: füge Skript ein, dass Gruppen automatisch berechnet
    group1 = snakemake.params.get("group1", "long=chrM,chr1,chr2,chr3,chr4,chr5,chr6,chr7,chr8,chr9,chr10,chr11,chr12,chr13,chr14,chr15,chr16,chr17,chr18,chr19,chr20,chr21,chr22,chrX,chrY,M,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,X,Y")
    group2 = snakemake.params.get("group2", "short=")
    # run shell
    shell("sleep 5;{script_coverage:q} \
        --alignmentFile={snakemake.input.bam:q} \
        --outputFile={snakemake.output.coverage:q} \
        --processors={snakemake.threads} \
        --basequalCutoff={qual_cutoff} \
        --ungappedSizes={snakemake.params.chrom_sizes:q}")
    shell("sleep 5;perl {script_groups:q} \
        {snakemake.output.coverage:q} \
        {group1} \
        {group2} > {snakemake.output.coverage_groups:q}")
except Exception as error:
    print(error)
    sys.exit(1)
