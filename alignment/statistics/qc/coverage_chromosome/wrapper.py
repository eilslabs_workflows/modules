"""Snakemake wrapper for coverage_chromosome (DKFZ script)"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os

try:
    script_coverage = "%s/genomeCoverage" % (snakemake.params.get("scriptdir", "scripts"))
    script_filter =  "%s/filter_readbins.pl" % (snakemake.params.get("scriptdir", "scripts"))
    # parameter
    window_size = snakemake.params.get("window_size", "10")
    # run shell
    shell("sleep 5;{script_coverage:q} \
        --alignmentFile={snakemake.input.bam:q} \
        --outputFile=/dev/stdout \
        --processors={snakemake.threads} \
        --mode=countReads \
        --windowSize={window_size} \
        | perl {script_filter:q} - {snakemake.params.chrom_sizes} > {snakemake.output.coverage:q}")
except Exception as error:
    print(error)
    sys.exit(1)
