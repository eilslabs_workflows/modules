"""Snakemake wrapper for bedtools coverage and DKFZ script"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os

try:
    script = "%s/targetcov.pl" % (snakemake.params.get("scriptdir", "scripts"))
    # load options
    samtools_options = snakemake.params.get("samtools_options", "-bu -q 1 -F 1024")
    coverage_options =  snakemake.params.get("coverage_options", "")
    # execute command
    shell("sleep 5;samtools view {samtools_options} {snakemake.input.bam:q} | " +
        "bedtools coverage {coverage_options} -abam stdin -b {snakemake.params.bed:q} | " +
        "perl {script:q} - > {snakemake.output.coverage:q}")
except Exception as error:
    print(error)
    sys.exit(1)

