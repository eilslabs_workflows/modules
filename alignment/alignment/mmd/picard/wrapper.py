"""Snakemake wrapper for mmd (merge and mark duplicates)"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell

try:
    java_options = snakemake.params.get("java_options", "")
    options = snakemake.params.get("options", "")
    bams = " ".join(["I='" + x + "'" for x in snakemake.input.get("bams")])
    log = snakemake.log_fmt_shell(stdout=False, stderr=True)
    shell("sleep 5;picard {java_options} MarkDuplicates QUIET=true VERBOSITY=ERROR {bams} O=/dev/stdout M={snakemake.output.metrics} {options} {log}" +
        "| samtools view -@ {snakemake.threads} -b - > {snakemake.output.bam:q}")
except Exception as error:
    print(error)
    sys.exit(1)
