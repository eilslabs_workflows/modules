"""Snakemake wrapper for bedtools intersect (exome)"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell

try:
    options = snakemake.params.get("options", "")
    shell("sleep 5;bedtools intersect {options} -abam {snakemake.input.bam:q} -b {snakemake.params.bed} > {snakemake.output.bam:q}")
except Exception as error:
    print(error)
    sys.exit(1)
