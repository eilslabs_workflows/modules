"""Snakemake wrapper for sort"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell

try:
    memory = snakemake.params.get("memory", "1G")
    log = snakemake.log_fmt_shell(stdout=False, stderr=True)
    shell("sleep 5;samtools view -uSbh {snakemake.input.sam:q}" +
        " | samtools sort -@ {snakemake.threads} -m {memory} -o - - > {snakemake.output.bam:q} {log}")
except Exception as error:
    print(error)
    sys.exit(1)