"""Snakemake wrapper for bisulfite/bconv"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os

try:
    script = "%s/bconv.py" % (snakemake.params.get("scriptdir", "scripts"))
    if snakemake.input.get("sam"):
        shell("sleep 5;samtools view -Sb {snakemake.input.sam:q} | python {script:q} -m {snakemake.output.metrics} - - | samtools sort - > {snakemake.output.bam:q}")
    if snakemake.input.get("bam"):
        shell("sleep 5;python {script:q} -m {snakemake.output.metrics} {snakemake.input.bam:q} - | samtools sort - > {snakemake.output.bam:q}")
    
except Exception as error:
    print(error)
    sys.exit(1)

