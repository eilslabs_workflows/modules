"""Snakemake wrapper for qc summary"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os

try:
    script = "%s/writeQCsummary.pl" % (snakemake.params.get("scriptdir", "scripts"))
    shell("sleep 5;perl {script:q} -p {snakemake.params.project:q} -s {snakemake.params.sample:q}" +
        " -r {snakemake.params.run:q} -l {snakemake.params.lane:q} -w {snakemake.log:q}" +
        " -f {snakemake.input.flagstat:q} -d {snakemake.input.diffchrom_statistics:q} -i {snakemake.input.isizes_statistics:q} " +
        " -c {snakemake.input.coverage_genome:q} > {snakemake.output.summary:q}")
except Exception as error:
    print(error)
    sys.exit(1)
