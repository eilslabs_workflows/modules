"""Snakemake wrapper for otp_json statistics (DKFZ script)"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os

try:
    script = "%s/qcJson.pl" % (snakemake.params.get("scriptdir", "scripts"))
    shell("sleep 5;perl {script:q} {snakemake.input.coverage_genome:q} {snakemake.input.coverage_groups:q}" +
        " {snakemake.input.isizes_statistics:q} {snakemake.input.flagstat:q} {snakemake.input.diffchrom_statistics:q}" +
        " > {snakemake.output.json:q}")
except Exception as error:
    print(error)
    sys.exit(1)
