rule analysis:
    input:
        bam = "data/alignment/{type_bamfile_id}.bam"
    output:
        diffchrom_matrix = "data/qc/{type_bamfile_id}_diffchrom_matrix.txt",
        diffchrom_statistics = "data/qc/{type_bamfile_id}_diffchrom_statistics.txt",
        isizes_matrix = "data/qc/{type_bamfile_id}_isizes_matrix.txt",
        isizes_statistic = "data/qc/{type_bamfile_id}_isizes_statistics.txt",
        extended_flagstat = "data/qc/{type_bamfile_id}_extendedFlagstat.txt"
    params:
        chrom_sizes = "data/reference/grch37_3regions/grch37_release-83_3regions_sizes.tab",
        insert_size_limit = 1000
    wrapper:
        "alignment/statistics/qc/bam_analysis"

rule flagstat:
    input:
        bam = "data/alignment/{type_bamfile_id}.bam"
    output:
        flagstat = "data/qc/{type_bamfile_id}_flagstat.txt"
    wrapper:
        "alignment/statistics/qc/flagstat"

rule coverage_genome:
    input:
        bam = "data/alignment/{type_bamfile_id}.bam"
    output:
        coverage = "data/qc/{type_bamfile_id}_coverage_genome.tab",
        coverage_groups = "data/qc/{type_bamfile_id}_coverage_genome_groups.tab"
    params:
        cores = 1,
        qual_cutoff = 1,
        chrom_sizes = "data/reference/grch37_3regions/grch37_release-83_3regions_sizes.tab",
        group1 = "long=chrA,chrB,chrC"
    wrapper:
        "alignment/statistics/qc/coverage_genome"

rule all:
    input:
        diffchrom_matrix = "data/qc/wgs/sample1_mmd_diffchrom_matrix.txt",
        diffchrom_statistics = "data/qc/wgs/sample1_mmd_diffchrom_statistics.txt",
        isizes_matrix = "data/qc/wgs/sample1_mmd_isizes_matrix.txt",
        isizes_statistic = "data/qc/wgs/sample1_mmd_isizes_statistics.txt",
        extended_flagstat = "data/qc/wgs/sample1_mmd_extendedFlagstat.txt",
        flagstat = "data/qc/wgs/sample1_mmd_flagstat.txt",
        coverage = "data/qc/wgs/sample1_mmd_coverage_genome.tab",
        coverage_groups = "data/qc/wgs/sample1_mmd_coverage_genome_groups.tab"