"""Snakemake wrapper for bwa index"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell

try:
    options = snakemake.params.get("options", "")
    shell("sleep 5;bwa index {options} -p {snakemake.params.prefix} {snakemake.input.fasta:q}")
except Exception as error:
    print(error)
    sys.exit(1)