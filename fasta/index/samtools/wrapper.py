"""Snakemake wrapper for samtools fasta index"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys, os
from snakemake.shell import shell

try:
    tmp_fasta = snakemake.output.get("fai")[:-4]
    if tmp_fasta != snakemake.input.get("fasta"):
        if os.path.exists(tmp_fasta): print("Warning: fasta file '" + tmp_fasta + "' already exists at index location")
        else:
            if tmp_fasta[0] == "/":
                print("Absolute path given: create softlink to fasta file at index location.")
                shell("sleep 5;ln -s {snakemake.input.fasta:q} '{tmp_fasta}'")
            else:
                print("Relative path given: copy fasta file to index location.")
                shell("sleep 5;cp {snakemake.input.fasta:q} '{tmp_fasta}'")
    shell("sleep 5;samtools faidx '{tmp_fasta}'")
    shell("sleep 5;cut -f1,2 {snakemake.output.fai:q} > {snakemake.output.sizes:q}")
except Exception as error:
    print(error)
    sys.exit(1)