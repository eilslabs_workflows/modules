"""Snakemake wrapper for bwa index"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell

try:
    shell("sleep 5;art_illumina {snakemake.params.options} -i {snakemake.input.fasta} -o {snakemake.params.prefix}")
except Exception as error:
    print(error)
    sys.exit(1)