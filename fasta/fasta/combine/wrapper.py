"""Snakemake wrapper for fasta/fasta/combine"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os
from Bio import SeqIO

try:
    with open(snakemake.output.get("fasta"), "w") as output_handle:
        for fasta in snakemake.input.get("fastas"):
            for record in SeqIO.parse(fasta, "fasta"):
                if snakemake.params.get("remove_description", True):
                    record.description=""
                SeqIO.write(record, output_handle, "fasta")
except Exception as error:
    print(error)
    sys.exit(1)