"""Snakemake wrapper for bisulfite/faconv"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os

try:
    script_faconf = "%s/faconv.py" % (snakemake.params.get("scriptdir", "scripts"))
    script_fapos = "%s/fapos.py" % (snakemake.params.get("scriptdir", "scripts"))
    shell("sleep 5;python {script_fapos:q} {snakemake.input.fasta:q} - | bgzip > {snakemake.output.positions:q}")
    shell("sleep 5;tabix -s 1 -b 2 -e 2 {snakemake.output.positions:q}")
    shell("sleep 5;python {script_faconf:q} {snakemake.input.fasta:q} {snakemake.output.fasta:q}")

except Exception as error:
    print(error)
    sys.exit(1)

