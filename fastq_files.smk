samples = {}

ruleorder: subsample_pe > art_pe > subsample_se > art_se

rule art_se:
    input:
        fasta = "data/reference/grch37_3regions/grch37_release-83_3regions.fasta"
    output:
        fastq = "{sample}.fq",
        aln = temp("{sample}.aln")
    params:
        options = "-ss HS25 -l 150 -f 10 -s 10",
        prefix = lambda wildcards: wildcards.sample
    wrapper:
        "fasta/fastq/art"

rule art_pe:
    input:
        fasta = "data/reference/grch37_3regions/grch37_release-83_3regions.fasta"
    output:
        fastq_1 = "{sample}1.fq",
        fastq_2 = "{sample}2.fq",
        aln_1 = temp("{sample}1.aln"),
        aln_2 = temp("{sample}2.aln")
    params:
        options = "-ss HS25 -p -l 150 -f 10 -m 200 -s 10",
        prefix = lambda wildcards: wildcards.sample
    wrapper:
        "fasta/fastq/art"

rule subsample_se:
    input:
       fastqs = ["{sample}_se.fq"]
    output:
        fastqs = [
            "{sample}_subsample_0.fq",
            "{sample}_subsample_1.fq",
            "{sample}_subsample_2.fq"]
    params:
        sample_size = "0.3",
        sample_number = "3",
        output_prefix = lambda wildcards: wildcards.sample + "_subsample"
    wrapper:
        "fastq/fastq/subsample"

rule subsample_pe:
    input:
       fastqs = ["{sample}_pe1.fq", "{sample}_pe2.fq"]
    output:
        fastqs = [
            "{sample}_subsample_0.1.fq",
            "{sample}_subsample_0.2.fq",
            "{sample}_subsample_1.1.fq",
            "{sample}_subsample_1.2.fq",
            "{sample}_subsample_2.1.fq",
            "{sample}_subsample_2.2.fq"]
    params:
        sample_size = "0.3",
        sample_number = "3",
        output_prefix = lambda wildcards: wildcards.sample + "_subsample"
    wrapper:
        "fastq/fastq/subsample"


rule zips:
    input:
        fq1 = "data/fastq/wgs/sample1_subsample_0.1.fq",
        fq2 = "data/fastq/wgs/sample1_subsample_0.2.fq"
    output:
        gz = "data/fastq/wgs/sample1_subsample_0.1.fq.gz",
        bz2 = "data/fastq/wgs/sample1_subsample_0.2.fq.bz2"
    shell:
        """
        gzip {input.fq1}
        bzip2 {input.fq2}
        """

rule all:
    input:
        "data/fastq/wgs/sample1_pe1.fq",
        "data/fastq/wgs/sample1_pe2.fq",
        "data/fastq/wgs/sample2_pe1.fq",
        "data/fastq/wgs/sample2_pe2.fq",
        "data/fastq/wgs/sample3_pe1.fq",
        "data/fastq/wgs/sample3_pe2.fq",
        "data/fastq/wgs/sample1_subsample_0.1.fq.gz",
        "data/fastq/wgs/sample1_subsample_0.2.fq.bz2",
        "data/fastq/wgs/sample1_subsample_1.1.fq",
        "data/fastq/wgs/sample1_subsample_1.2.fq",
        "data/fastq/wgs/sample1_subsample_2.1.fq",
        "data/fastq/wgs/sample1_subsample_2.2.fq",
        "data/fastq/wgs/sample4_se.fq",
        "data/fastq/wgs/sample5_se.fq",
        "data/fastq/wgs/sample6_se.fq",
        "data/fastq/wgs/sample4_subsample_0.fq",
        "data/fastq/wgs/sample4_subsample_1.fq",
        "data/fastq/wgs/sample4_subsample_2.fq"

        