
rule fqconv:
    input:
        fastq = "data/fastq/wgs/{sample_subsample}.fq"
    output:
        fastq = "data/fastq/wgs/{sample_subsample}_fqconv.fq"
    params:
        type = "SE"
    wrapper:
        "fastq/fastq/methylCtools/fqconv"

rule align:
    input: 
        fastqs = ["data/fastq/wgs/{sample_subsample}_fqconv.fq"]
    output:
        sam = "data/alignment/wgbs/{sample_subsample}.sam"
    params:
        reference = "data/reference/grch37_3regions/grch37_release-83_3regions_faconv",
        options = "-T 0"
    threads: 8
    log:
        "logs/{sample_subsample}_bwa.log"
    wrapper:
        "fastq/alignment/bwa"

rule sort:
    input:
        sam = "data/alignment/wgbs/{sample_subsample}.sam"
    output:
        bam = "data/alignment/wgbs/{sample_subsample}.bam"
    threads: 8
    log:
        "logs/sort_{sample_subsample}.log"
    wrapper:
        "alignment/alignment/sort/samtools"

rule bconv:
    input:
        sam = "data/alignment/wgbs/{sample_subsample}.sam"
    output:
        bam = "data/alignment/wgbs/{sample_subsample}_bconv.bam",
        metrics = "data/alignment/wgbs/{sample_subsample}_bi.metrics.txt"
    wrapper:
        "alignment/alignment/methylCtools/bconv"


rule index:
    input:
        bam = "{filename}.bam"
    output:
        bai = "{filename}.bai"
    wrapper:
        "alignment/index/samtools"


def all_input(wildcards):
    files = []
    path = "data/alignment/wgbs/"
    sample = "sample4"
    for i in ["0","1","2"]:
        files.append(path + sample + "_subsample_"+ i + "_bconv.bam")
        files.append(path + sample + "_subsample_"+ i + "_bconv.bai")
        files.append(path + sample + "_subsample_"+ i + ".bam")
    return(files)

rule all:
    input:
        all_input