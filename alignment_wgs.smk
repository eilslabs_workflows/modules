rule stream_gz:
    input:
        fastq = "data/fastq/wgs/{sample_subsample_num}.fq.gz"
    output:
        fastq = temp("data/fastq/wgs/{sample_subsample_num}.fq")
    wrapper:
        "fastq/fastq/stream"

rule stream_bz2:
    input:
        fastq = "data/fastq/wgs/{sample_subsample_num}.fq.bz2"
    output:
        fastq = temp("data/fastq/wgs/{sample_subsample_num}.fq")
    wrapper:
        "fastq/fastq/stream"

rule align:
    input: 
        fastqs = ["data/fastq/wgs/{sample_subsample}.1.fq", "data/fastq/wgs/{sample_subsample}.2.fq"]
    output:
        sam = "data/alignment/wgs/{sample_subsample}.sam"
    params:
        reference = "data/reference/grch37_3regions/grch37_release-83_3regions",
        options = "-T 0"
    threads: 8
    log:
        "logs/{sample_subsample}_bwa.log"
    wrapper:
        "fastq/alignment/bwa"

rule sort:
    input:
        sam = "data/alignment/wgs/{sample_subsample}.sam"
    output:
        bam = "data/alignment/wgs/{sample_subsample}.bam"
    threads: 8
    log:
        "logs/sort_{sample_subsample}.log"
    wrapper:
        "alignment/alignment/sort/samtools"


def mmd_input(wildcards):
    files = []
    for i in ["0","1","2"]:
        files.append("data/alignment/wgs/sample1_subsample_"+ i + ".bam")
    return(files)

rule mmd:
    input:
        bams = mmd_input
    output:
        bam = "data/alignment/wgs/sample1_mmd.bam",
        metrics = "data/alignment/wgs/sample1_mmd_pic.metrics"
    params:
        options = "COMPRESSION_LEVEL=0 VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=TRUE CREATE_INDEX=FALSE MAX_RECORDS_IN_RAM=12500000",
        java_options = "-Xms4G -Xmx4G",
        memory = "8G"
    threads: 8
    log:
        "logs/mmd_picard_markduplicates_ad.log"
    wrapper:
        "alignment/alignment/mmd/picard"

rule intersect:
    input:
        bam = "data/alignment/wgs/sample1_mmd.bam"
    output:
        bam = "data/alignment/wgs/sample1_mmd_intersect.bam"
    params:
        options = "",
        bed = "data/reference/grch37_3regions/target_regions.bed"
    wrapper:
        "alignment/alignment/intersect"

rule index:
    input:
        bam = "{filename}.bam"
    output:
        bai = "{filename}.bai"
    wrapper:
        "alignment/index/samtools"


rule all:
    input:
        bam1 = "data/alignment/wgs/sample1_mmd.bam",
        bai1 = "data/alignment/wgs/sample1_mmd.bai",
        bam2 = "data/alignment/wgs/sample1_mmd_intersect.bam",
        bai2 = "data/alignment/wgs/sample1_mmd_intersect.bai"
        