# get chromosome 17 fastq file and extract regions

rule GRCh37_chromosome_wget:
    output:
        fasta = temp("Homo_sapiens.GRCh37.dna.chromosome.17.fa.gz")
    shell:
        """
        wget http://ftp.ensembl.org/pub/grch37/release-83/fasta/homo_sapiens/dna/{output.fasta}
        """

rule GCRCh37_chromosome_unzip:
    input:
        fasta = "Homo_sapiens.GRCh37.dna.chromosome.17.fa.gz"
    output:
        fasta = temp("Homo_sapiens.GRCh37.dna.chromosome.17.fa")
    shell:
        """
        gzip -d {input.fasta}
        """

#requires biopython
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord

rule small_reference:
    input:
        grch37_chr17 = "Homo_sapiens.GRCh37.dna.chromosome.17.fa"
    output:
        grch37_3regions = "data/reference/grch37_3regions/grch37_release-83_3regions.fasta"
    run:
        record_dict = SeqIO.index(input.grch37_chr17, "fasta")
        with open(output.grch37_3regions, "w") as output_handle:
            SeqIO.write(SeqRecord(record_dict["17"].seq[7563097:7567097], "chrA", "", ""), output_handle, "fasta")
            SeqIO.write(SeqRecord(record_dict["17"].seq[41194312:41198312], "chrB", "", ""), output_handle, "fasta")
            SeqIO.write(SeqRecord(record_dict["17"].seq[76212267:76216267], "chrC", "", ""), output_handle, "fasta")

rule small_reference_targets:
    output:
        grch37_3regions_targets = "data/reference/grch37_3regions/target_regions.bed"
    run:
        with open(output.grch37_3regions_targets, 'w') as f:
            f.write("chrA\t1\t4000\n")
            f.write("chrB\t1\t4000\n")
            f.write("chrC\t1\t4000\n")


# create index
rule bwa_index:
    input:
        fasta = "{reference}.fasta"
    output:
        bwt = "{reference}.bwt"
    params:
        prefix = lambda wildcards: wildcards.reference,
        options = "-a bwtsw"
    wrapper:
        "fasta/index/bwa"

# facov
rule reference_faconv:
    input:
        fasta = "data/reference/grch37_3regions/grch37_release-83_3regions.fasta"
    output:
        fasta = "data/reference/grch37_3regions/grch37_release-83_3regions_faconv.fasta",
        positions = "data/reference/grch37_3regions/grch37_release-83_3regions.positions.gz"
    wrapper:
        "fasta/fasta/methylCtools/faconv"

# fasta index
rule index:
    input:
        fasta = "{fastafile}.fasta"
    output:
        fai = "{fastafile}.fasta.fai",
        sizes = "{fastafile}_sizes.tab"
    wrapper:
        "fasta/index/samtools"

# fingerprinting
rule small_reference_fingerprinting:
    output:
        grch37_3regions_fingerprinting = "data/reference/grch37_3regions/fingerprinting.bed"
    run:
        with open(output.grch37_3regions_fingerprinting, 'w') as f:
            f.write("chrA\t284\t285\trstmp1\t0\t+\tA\tG\n")
            f.write("chrA\t284\t285\trstmp2\t0\t+\tC\tT\n")
            f.write("chrB\t1023\t1024\trstmp3\t0\t+\tA\tG\n")
            f.write("chrB\t1023\t1024\trstmp4\t0\t+\tC\tT\n")
            f.write("chrC\t12\t13\trstmp5\t0\t+\tA\tG\n")
            f.write("chrC\t12\t13\trstmp6\t0\t+\tC\tT\n")

rule all:
    input:
        "data/reference/grch37_3regions/grch37_release-83_3regions.fasta",
        "data/reference/grch37_3regions/grch37_release-83_3regions.bwt",
        "data/reference/grch37_3regions/grch37_release-83_3regions.fasta.fai",
        "data/reference/grch37_3regions/target_regions.bed",
        "data/reference/grch37_3regions/grch37_release-83_3regions_faconv.fasta",
        "data/reference/grch37_3regions/grch37_release-83_3regions_faconv.bwt",
        "data/reference/grch37_3regions/grch37_release-83_3regions_faconv.fasta.fai",
        "data/reference/grch37_3regions/fingerprinting.bed"
