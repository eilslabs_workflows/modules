import subprocess
import sys
import argparse
import os 


# settings

workdir = os.path.dirname(os.path.realpath(__file__))

parser = argparse.ArgumentParser(description="This script performs tests of all modules and downloads also required conda environments.")
parser.add_argument('--module', type=str, help="Test specific module", required=False)
args = parser.parse_args()

datadir = workdir + "/data"

# define modules

modules = [
    "alignment/alignment/methylCtools/bconv",
    "alignment/alignment/mmd/picard",
    "alignment/alignment/sort/samtools",
    "alignment/alignment/intersect",
    "alignment/index/samtools",
    "alignment/statistics/exome/coverage",
    "alignment/statistics/qc/bam_analysis",
    "alignment/statistics/qc/coverage_chromosome",
    "alignment/statistics/qc/coverage_genome",
    "alignment/statistics/qc/fingerprinting",
    "alignment/statistics/qc/flagstat",
    "alignment/variants/methylCtools/bcall",
    "fastq/alignment/bwa",
    "fastq/fastq/convert",
    "fastq/fastq/methylCtools/fqconv",
    "fastq/fastq/subsample",
    "fastq/fastq/trimmomatic",
    "fastq/fastq/stream",
    "fasta/fasta/combine",
    "fasta/fasta/methylCtools/faconv",
    "fasta/index/bwa",
    "fasta/index/samtools",
    "fasta/fastq/art",
    "undefined/undefined/mbuffer",
    "undefined/undefined/sra_tools",
    "statistics/statistics/qc/otp_json",
    "statistics/statistics/qc/summary"
]

def run_module(module, workdir, datadir):
    print(">>>>>>>>>> Run test: " + module)
    subprocess.run(
        [
           "snakemake",
            "--force",
            "--forceall",
            "--cores", "3",
            "--use-conda",
            "--conda-prefix", "%s/conda/" % (workdir),
            "--config", "data=%s" % (datadir), 
            "--snakefile", "%s/%s/Snakefile" % (workdir, module),
            "--wrapper-prefix", "file://%s/" % workdir,
            "--output-wait", "20",
            "--latency-wait", "20",
            "all"
        ]
    )

def main():
    try:
        if args.module in modules:
            run_module(args.module, workdir, datadir)
        else:
            for module in modules:
                print(module)
                run_module(module, workdir, datadir)
    except NameError:
        print(module)
    except Exception as error:
        print(error)
        sys.exit(1)

main()



