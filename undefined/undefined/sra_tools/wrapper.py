"""Snakemake wrapper for sra-tools"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import os

def get_outdir(fastqfile):
    outdir = os.path.dirname(fastqfile)
    if outdir == "":
        outdir = os.getcwd()
    elif outdir[0] != "/":
        outdir = os.getcwd() + "/" + outdir
    return(outdir)

try:
    if snakemake.params.get("type") == "fastq_paired":
        outdir = get_outdir(snakemake.output.get("fastq_1"))
        shell("sleep 5;fastq-dump --split-files --origfmt  --bzip2 -O {outdir:q} {snakemake.params.id}")
    if snakemake.params.get("type") == "fastq_single":
        outdir = get_outdir(snakemake.output.get("fastq"))
        shell("sleep 5;fastq-dump --origfmt --bzip2 -O {outdir:q} {snakemake.params.id}")
except Exception as error:
    print(error)
    sys.exit(1)