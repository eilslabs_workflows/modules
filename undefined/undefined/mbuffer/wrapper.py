"""Snakemake wrapper for mbuffer"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
from snakemake.utils import format

try:
    size = snakemake.params.get("size", "100M")
    output_files = " ".join(snakemake.output.get("files"))
    print(output_files)
    #log = snakemake.log_fmt_shell(stdout=True, stderr=True)
    shell("sleep 5;cat {snakemake.input.file:q} | mbuffer -m {size} | tee {output_files} > /dev/null")
except Exception as error:
    print(error)
    sys.exit(1)