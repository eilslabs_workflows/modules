"""Snakemake wrapper for fastq/fastq/methylCtools/fqconv"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os

try:
    script = "%s/fqconv.py" % (snakemake.params.get("scriptdir", "scripts"))
    if snakemake.params.get("type") == "PE":
        fastqs = "-1 '" + snakemake.input.get("fastqs")[0] + "' -2 '" + snakemake.input.get("fastqs")[1] + "'"
    if snakemake.params.get("type") == "SE":
        fastqs = "-1 '" + snakemake.input.get("fastq") + "'"
    shell("sleep 5;python {script:q} {fastqs} {snakemake.output.fastq:q}")

except Exception as error:
    print(error)
    sys.exit(1)

