"""Snakemake wrapper for trimmomatic"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell

try:
    log = snakemake.log_fmt_shell(stdout=True, stderr=True)
    options = snakemake.params.get("options")
    trimmer = snakemake.params.get("trimmer")
    if hasattr(snakemake.input, 'fastq'):
        shell("sleep 5;trimmomatic {options} -threads {snakemake.threads} {snakemake.input.fastq:q} {snakemake.output.fastq:q} {trimmer} {log}")
    elif hasattr(snakemake.input, 'fastq1') & hasattr(snakemake.input, 'fastq2'):
        shell("sleep 5;trimmomatic {options} -threads {snakemake.threads} " +
            "{snakemake.input.fastq1:q} {snakemake.input.fastq2:q} {snakemake.output.fastq1:q} " +
            "{snakemake.output.fastq1_unpaired:q} {snakemake.output.fastq2:q} {snakemake.output.fastq2_unpaired:q} {trimmer} {log}")
except Exception as error:
    print(error)
    sys.exit(1)