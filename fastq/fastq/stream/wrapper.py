"""Snakemake wrapper for fastq stream"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from os import path
from snakemake.shell import shell

try:
    extension = path.splitext(snakemake.input.get("fastq"))[1]
    assert extension in [".gz", ".bz2", ".fastq", ".fq"]
    if (extension == ".gz"):
        shell("sleep 5;gzip -cd {snakemake.input.fastq:q} > {snakemake.output.fastq:q}")
    elif (extension == ".bz2"):
        shell("sleep 5;bzcat {snakemake.input.fastq:q} > {snakemake.output.fastq:q}")
    elif (extension in [".fastq", ".fq"]):
        shell("sleep 5;cat {snakemake.input.fastq:q} > {snakemake.output.fastq:q}")

except AssertionError as error:
    print("Input file extension in not supported. Please provide FASTQ files as '.gz', .'bz2', '.fastq' or '.fq'")
    sys.exit(1)

except Exception as error:
    print(error)
    sys.exit(1)

