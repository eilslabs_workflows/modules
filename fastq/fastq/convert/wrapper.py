"""Snakemake wrapper for fastq convert"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell
import urllib.request, os

try:
    script = "%s/Illumina2PhredScore.pl" % (snakemake.params.get("scriptdir", "scripts"))
    shell("sleep 5;perl {script:q} {snakemake.input.fastq:q} > {snakemake.output.fastq:q}")
except Exception as error:
    print(error)
    sys.exit(1)
