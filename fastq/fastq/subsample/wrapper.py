"""Snakemake wrapper for subsample"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
import urllib.request, os
from snakemake.shell import shell

try:
    script = "%s/randomReadSubSample.py" % (snakemake.params.get("scriptdir", "scripts"))
    fastqs = snakemake.input.get("fastqs")
    if len(fastqs) == 2:
        shell("sleep 5;python {script:q} -f1 {fastqs[0]:q} -f2 {fastqs[1]:q} -s {snakemake.params.sample_size} -n {snakemake.params.sample_number} -o {snakemake.params.output_prefix:q}")
    if len(fastqs) == 1:
        shell("sleep 5;python {script:q} -f {fastqs[0]:q} -s {snakemake.params.sample_size} -n {snakemake.params.sample_number} -o {snakemake.params.output_prefix:q}")

except Exception as error:
    print(error)
    sys.exit(1)

