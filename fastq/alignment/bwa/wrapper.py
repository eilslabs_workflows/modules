"""Snakemake wrapper for bwa mem"""

__author__ = "Sven Twardziok"
__copyright__ = "Copyright 2019, Charité Universitätsmedizin Berlin"
__email__ = "sven.twardziok@charite.de"
__license__ = "MIT"

import sys
from snakemake.shell import shell

class BwaLogError(Exception):
    """Exception raised for errors in the bwa logfile.

    Attributes:
        line -- line in which the error occurred
        message -- explanation of the error
        code -- error code
    """

    def __init__(self, message, line, code):
        self.message = message
        self.line = line
        self.code = code


try:
    options = snakemake.params.get("options", "-T 0")
    reference = snakemake.params.get("reference")
    log = snakemake.log_fmt_shell(stdout=False, stderr=True)
    fastqs = " ".join(list(map(lambda x: "'" + x + "'",snakemake.input.get("fastqs"))))
    shell("sleep 5;bwa mem {options} -t {snakemake.threads} {reference:q} {fastqs} > {snakemake.output.sam:q} {log}")
    with open(str(snakemake.log), "r") as logfile:
        for line in logfile:
            if "fault" in line:
                raise BwaLogError("Found segfault in bwa logfile!", line, 31)
            if "error" in line.lower() or "error_count" in line or "dummy be execution" in line:
                raise BwaLogError("Found error in bwa logfile!", line, 36)
            if "Abort. Sorry." in line:
                raise BwaLogError("Found error in bwa logfile!", line, 37)
except BwaLogError as error:
    print("BWA log exception: ", error.message)
    print(error.line)
    sys.exit(error.code)
except Exception as error:
    print(error)
    sys.exit(1)